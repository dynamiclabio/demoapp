package dynamiclabio.demoapp

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.AppCompatTextView
import android.util.Log
import android.view.View
import io.dynamiclab.junitlistener.JUnitReportListener
import org.hamcrest.Description
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import android.widget.EditText
import android.widget.TextView
import org.hamcrest.Matcher
import org.hamcrest.StringDescription
import org.hamcrest.TypeSafeMatcher


@RunWith(AndroidJUnit4::class)
class MainActivityEspressoTest {

    private val TAG = "EsspresoTest"


    @Before
    fun init() {
    }

    @Rule
    @JvmField
    val mainActivityRule = IntentsTestRule(MainActivity::class.java)


    @Test
    fun basicTest() {
        Log.i(TAG, "Start basicTest")
        onView(withId(R.id.welcomeTextView)).check(matches(hasValueEqualTo("Hello World!")))
        onView(withId(R.id.updateTextButton)).perform(click())
        Thread.sleep(3000)
        JUnitReportListener.takeScreenshot(mainActivityRule.getActivity());
        onView(withId(R.id.welcomeTextView)).check(matches(hasValueEqualTo("Hello Kotlin World!")))
    }


    @Test
    fun checkboxTest() {
        onView(withId(R.id.checkBox1)).perform(click())
        Thread.sleep(3000)
        JUnitReportListener.takeScreenshot(mainActivityRule.getActivity());
        onView(withId(R.id.welcomeTextView)).check(matches(hasValueEqualTo("CheckBox ON")))
        onView(withId(R.id.checkBox1)).perform(click())
        Thread.sleep(3000)
        JUnitReportListener.takeScreenshot(mainActivityRule.getActivity());
        onView(withId(R.id.welcomeTextView)).check(matches(hasValueEqualTo("CheckBox OFF")))
    }


    @Test
    fun switchTest() {
        onView(withId(R.id.switch1)).perform(click())
        onView(withId(R.id.welcomeTextView)).check(matches(hasValueEqualTo("Switch ON")))
        onView(withId(R.id.switch1)).perform(click())
        onView(withId(R.id.welcomeTextView)).check(matches(hasValueEqualTo("Switch OFF")))
    }



    @Test
    fun switchShouldFailTest() {
        onView(withId(R.id.switch1)).perform(click())
        onView(withId(R.id.welcomeTextView)).check(matches(hasValueEqualTo("Switch OFF")))
        Espresso.pressBack();
    }


    fun hasValueEqualTo(content: String): Matcher<View> {

        return object : TypeSafeMatcher<View>() {

            override fun describeTo(description: Description) {
                description.appendText("$content")
            }

            override fun matchesSafely(view: View?): Boolean {
                if (view !is TextView && view !is EditText && view !is AppCompatTextView) {
                    return false
                }
                if (view != null) {
                    val text: String
                    if(view is AppCompatTextView){
                        text = (view as AppCompatTextView).text.toString()
                    }else if(view is EditText) {
                        text = (view as EditText).text.toString()
                    } else {
                        text = (view as TextView).text.toString()
                    }

                    var res = text.equals(content, ignoreCase = true)
                    return res
                }
                return false
            }
        }
    }


}