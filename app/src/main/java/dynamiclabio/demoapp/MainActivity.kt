package dynamiclabio.demoapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        switch1.setOnCheckedChangeListener{ buttonView, isChecked ->
            if (isChecked) {
                welcomeTextView.text = "Switch ON"
            } else {
                welcomeTextView.text = "Switch OFF"
            }
        }
        checkBox1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                welcomeTextView.text = "CheckBox ON"
            } else {
                welcomeTextView.text = "CheckBox OFF"
            }
        }
    }

    fun click (view: View) {
        welcomeTextView.text = "Hello Kotlin World!"
    }

}
